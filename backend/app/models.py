import django
from django.db import models

from django.conf import settings
import django.utils.timezone
from django.utils.timezone import activate

import hashlib
import json
import os
from PIL import Image

ICT =  django.utils.timezone.get_fixed_timezone(420)
activate(ICT)

def generate_filename(idname, filename):
    json_str = json.dumps([idname])

    return '{0}.{1}'.format(hashlib.md5(json_str.encode()).hexdigest(), filename.split('.')[-1])

def update_filename(instance, filename):
    path = "users/"
    return '{0}{1}'.format(path, filename)

# Create your models here.
class Member(models.Model):
    id = models.AutoField(primary_key=True)
    idname = models.CharField(max_length=256, unique=True)
    idpass = models.CharField(max_length=32, unique=True)
    email = models.CharField(max_length=512, unique=True)
    password = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    gender = models.BooleanField(default=True)
    image = models.ImageField(upload_to=update_filename, default = 'users/not-img.jpg')
    pub_date = models.DateTimeField('date published', default=django.utils.timezone.now)

    def save(self, *args, **kwargs):
        self.image.name = generate_filename(self.idname, self.image.name)
        try:
            image = Image.open(self.image)
            (width, height) = image.size
            is_new_image = width > 120 or height > 120
        except FileNotFoundError:
            is_new_image = False
        if is_new_image:
            try:
                os.remove(os.path.join(settings.MEDIA_ROOT, 'users/{0}'.format(self.image.name)))
            except OSError:
                pass
        super(Member, self).save(*args, **kwargs)
        if is_new_image:
            self.resize_image(image)

    def resize_image(self, image):
            (width, height) = image.size
            if width < height:
                factor = (height / 120) * 100
            else:
                factor = (width / 120) * 100
            new_width = width * 100 / factor
            if new_width < 1:
                new_width = 1
            new_height = height * 100 / factor
            if new_height < 1:
                new_height = 1
            size = (int(new_width), int(new_height))
            image = image.resize(size, Image.ANTIALIAS)
            image.save(self.image.path)

    def __str__(self):
        return "{0} ({1})".format(self.email, self.idname)
